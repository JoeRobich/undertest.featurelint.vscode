# UnderTest.Featurelint VS

[![Version](https://vsmarketplacebadge.apphb.com/version/undertest.undertest-featurelint.svg)](https://marketplace.visualstudio.com/items?itemName=undertest.undertest-featurelint) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/undertest.undertest-featurelint.svg)](https://marketplace.visualstudio.com/items?itemName=undertest.undertest-featurelint) [![Downloads](https://vsmarketplacebadge.apphb.com/downloads-short/undertest.undertest-featurelint.svg)](https://marketplace.visualstudio.com/items?itemName=undertest.undertest-featurelint) 

> Feature/Gherkin linting and style checking for Visual Studio Code

## Features

* Executes linters against `*.feature` (gherkin) files.
* Current rules are:
    * Bad Phrase (phrases which should not be in high quality features)
    * Data Table Headers Use Pascal Case
    * Ensure feature names are unique
    * Ensure scenario names are unique
    * Feature name required
    * No duplicate tags
    * No empty feature files
    * No periods at the end of lines
    * No `Then`s in backgrounds 
    * No upper case words
    * Ensure order of `Given-When-Then`
    * Scenario name required
    * Scenario outline examples should have a name
    * Scenario outline should have an example table
    * Ensure example elements are mapped rul

![](https://gitlab.com/under-test/undertest.featurelint.vscode/raw/stable/img/FeatureForReadme.png)

## Requirements

* VS Code (>= 0.10.1)
* [UnderTest.FeatureLint](https://gitlab.com/under-test/undertest.featurelint)

## Installing

1. Press Ctrl+Shift+X to open the Extensions tab
1. Type `undertest` to find the extension
1. Click the Install button, then the Enable button

OR

1. Open Visual Studio Code
1. Press Ctrl+P to open the Quick Open dialog
1. Type `ext install undertest.undertest-featurelint` to find the extension
1. Click the Install button, then the Enable button

OR

1. Open a command-line prompt
1. Run code --install-extension undertest.undertest-featurelint

## Extension Settings

None currently.  If you have a request for one, [please request](https://gitlab.com/under-test/undertest.featurelint.vscode/issues).

## Changelog

The full changelog is available here: [changelog](https://gitlab.com/under-test/undertest.featurelint.vscode/blob/stable/CHANGELOG.md).

## Known Issues

Linting is currently limited to single file rules.
