# Change Log

All notable changes to the "UnderTest.FeatureLint".

## 0.0.25 (2019-11-13)
### changed
- corrections to prefixes in snippets

## 0.0.24 (2019-11-12)
### added
- support for initial snippets

## 0.0.23 (2019-08-18)
### added
- extension bundling

## 0.0.20 (2019-07-30)
### changed
* updates README with all of the current rules #22

## 0.0.20 (2019-07-29)
### changed
* updates to packages

## 0.0.19 (2019-07-29)
### added
* adds an auto updater for featurelint #19

## 0.0.18 (2019-07-27)
### changed
* updates README.md to include links to the marketplace

## 0.0.17 (2019-05-08)
### added
* adds feedback command `UnderTest: I have feedback`
### changed
* updates CHANGELOG to include release dates

## 0.0.16 (2019-05-08)
### added
* add [silverbulleters.gherkin-autocomplete](https://marketplace.visualstudio.com/items?itemName=silverbulleters.gherkin-autocomplete) as an extension dependency.
### changed
* corrected issue with README formatting

## 0.0.15 (2019-04-17)
### added
* add [alexkrechik.cucumberautocomplete](https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete) as an extension dependency.

## 0.0.14 (2019-04-17)
### added
* when [featurelint](https://www.nuget.org/packages/UnderTest.FeatureLint) is not installed we now install it.
### changes
* updated the check for if featurelint is installed to use featurelint vs hasbin
* removed hasbin as dependency

## 0.0.13 (2019-04-16)
### added
* added runtime dependency on [ms-vscode.csharp](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp) to ensure we have `dotnet` CLI available.

## 0.0.12 (2019-04-16)
### changes
- Improved error message when featurelint is not installed

## 0.0.11 (2019-04-09)
### changes
- Improved interaction with other extensions

## 0.0.5 - 0.0.8 (2019-04-09)
### changes
- Name update to `Gherkin Linter (UnderTest.FeatureLint)`
- Fix around document selector not being consistent 
- Documentation improvements in README

## 0.0.4 (2019-04-08)
- Update the document selector to `feature` over `gherkin`

## 0.0.3 (2019-04-07)
- Improve README

## 0.0.2 (2019-04-07)
- Improve README

## 0.0.1 (2019-04-07)
- Initial release
