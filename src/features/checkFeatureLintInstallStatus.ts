import * as vscode from 'vscode';
import * as cp from 'child_process';
import * as semver from 'semver';
import * as req from 'request-promise';

export async function checkIfFeatureLintNeedsInstallingOrUpgrading(): Promise<boolean>
{
  try
  {
    const installedVersion = await isFeatureLintInstalled();
    if (!installedVersion === undefined)
    {
      displayFeatureLintNotAvailable();
      return false;
    }

    let currentVersion = parseCurrentVersionFromCliResult(installedVersion.toString());
    let latestVersion = await getLatestVersionNumber();

    if (semver.gte(currentVersion, latestVersion))
    {
      return true;
    }
  }
  catch (error)
  {
    displayFeatureLintNotAvailable();      
    return false;
  }  

  reinstallFeatureLint();
  return true;
}

export function displayFeatureLintNotAvailable()
{
  var message = `FeatureLint not found.  And is required.`;
  vscode.window.showInformationMessage(message, {  }, { title: `Install` }).then(selection => {
    if (!selection)
    {
      return;
    }

    installFeatureLint();
  });
}

async function isFeatureLintInstalled(): Promise<string>
{
  const versionCommand = `featurelint --version`;

  return new Promise((resolve, reject) => {
    execPromise(versionCommand)
    .then((result: string) => {
      resolve(result); 
    })
    .catch((result) => resolve(result));
  });
}

function execPromise(command: string) : Promise<string> 
{
  return new Promise<string>(function(resolve, reject) 
  {
    cp.exec(command, (error, stdout, stderr) => 
    {
      if (error || stderr.length > 0) 
      {
        reject(stderr.trim());
        return;
      }

      resolve(stdout.trim());
    });
  });
}

async function getLatestVersionNumber() : Promise<string>
{
  const url = `https://api-v2v3search-0.nuget.org/query?q=packageid:undertest.featurelint`;
  var options = {
    uri: url,
    json: true 
  };
 
  return req(options)
    .then(function (result) {
      var versions = result.data[0].versions;
      return versions[versions.length - 1].version;
    })
    .catch(function (err) {
      return null;
    });
}

function installFeatureLint()
{
  try
  {
    cp.execSync(`dotnet tool install -g UnderTest.FeatureLint`);
    vscode.window.showInformationMessage(`Successfully install UnderTest.FeatureLint`);   
    promptToReloadWindow();    
  }
  catch(error)
  {
    vscode.window.showInformationMessage(`Failed to install with error: ${error.message}`);   
  }
}

function reinstallFeatureLint()
{
  try
  {
    cp.execSync(`dotnet tool uninstall -g UnderTest.FeatureLint`);
    cp.execSync(`dotnet tool install -g UnderTest.FeatureLint`);
    vscode.window.showInformationMessage(`Successfully upgraded UnderTest.FeatureLint`);   
    promptToReloadWindow(`Successfully upgraded UnderTest.FeatureLint.\n`);    
  }
  catch(error)
  {
    vscode.window.showInformationMessage(`Failed to install with error: ${error.message}`);   
  }
}

function parseCurrentVersionFromCliResult(versionOutput: string)
{
  return versionOutput
    .replace(/(.*)FeatureLint /, '')
    .replace(/(.*)UnderTest /, '')
    .trim();
}

function promptToReloadWindow(message: string = '')
{
  const action = 'Reload';
  vscode.window
    .showInformationMessage(`${message}Reload window for FeatureLint to be available.`, action)
    .then(selection => 
    {
      if (selection === action) 
      {
        vscode.commands.executeCommand('workbench.action.reloadWindow');
      }
    });  
}
 