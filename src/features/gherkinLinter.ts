import * as vscode from 'vscode';

import * as cp from 'child_process';
import { Constants } from '../constants';
import { FeatureLintData } from '../FeatureLintCli/FeatureLintData';
import { FeatureLintResponse } from '../FeatureLintCli/FeatureLintResponse';

export default class GherkinLinter implements vscode.CodeActionProvider 
{
  private static commandId: string = 'gherkin.runCodeAction';
  private command!: vscode.Disposable;
  private diagnosticCollection!: vscode.DiagnosticCollection;

  public provideCodeActions(document: vscode.TextDocument, 
                            range: vscode.Range, 
                            context: vscode.CodeActionContext, 
                            token: vscode.CancellationToken): vscode.Command[] 
  {
    // for later when when have suggestions
    /*let diagnostic:vscode.Diagnostic = context.diagnostics[0];
		return [{
			title: "Accept featurelint suggestion",
			command: GherkinLinter.commandId,
			arguments: [document, diagnostic.range, diagnostic.message]
    }];*/
    return [];
	}

  public activate(subscriptions: vscode.Disposable[]) 
  {
    
		this.command = vscode.commands.registerCommand(GherkinLinter.commandId, this.runCodeAction, this);
		subscriptions.push(this);
		this.diagnosticCollection = vscode.languages.createDiagnosticCollection();

    vscode.workspace.onDidOpenTextDocument(this.updateDiagnosticsFromFeatureLint, this, subscriptions);
    
    vscode.workspace.onDidSaveTextDocument(this.updateDiagnosticsFromFeatureLint, this);

		vscode.workspace.onDidCloseTextDocument((textDocument) => {
			this.diagnosticCollection.delete(textDocument.uri);
		}, null, subscriptions);

		// lint all open gherkin documents
		vscode.workspace.textDocuments.forEach(this.updateDiagnosticsFromFeatureLint, this);
	}

  public dispose(): void 
  {
		this.diagnosticCollection.clear();
		this.diagnosticCollection.dispose();
		this.command.dispose();
	}

  private updateDiagnosticsFromFeatureLint(textDocument: vscode.TextDocument) 
  {
    if (textDocument.languageId !== Constants.TargetLanguageId) 
    {
			return;
		}
		
		let decoded = '';
		const diagnostics: vscode.Diagnostic[] = [];

		let options = vscode.workspace.rootPath ? { cwd: vscode.workspace.rootPath } : undefined;
		let args =  ['--json', textDocument.fileName];
		
		let childProcess = cp.spawn('featurelint', args, options);
		childProcess.on('error', (error: Error) => {
      // ok - featurelint is installed - output the returned error message
      vscode.window.showInformationMessage(`Cannot featurelint the gherkin file with error: ${error.message}`);
    });
     
    if (childProcess.pid) 
    {
			childProcess.stdout.on('data', (data: Buffer) => {
        decoded += data;
      });
      
			childProcess.stdout.on('end', () => {
        const result: FeatureLintResponse = JSON.parse(decoded);
        if (result.Successful || result.Results.length === 0)
        {
          this.diagnosticCollection.set(textDocument.uri, []);	  
          return;
        }

        result.Results[0].Findings.forEach((item: FeatureLintData) => {
					let severity = item.Type === 2 ? vscode.DiagnosticSeverity.Warning : vscode.DiagnosticSeverity.Error;
					let message = item.Message;
					let range = new vscode.Range(Math.max(item.Line - 1, 0), item.Column, Math.max(item.Line - 1, 0), item.Column);
					let diagnostic = new vscode.Diagnostic(range, message, severity);
					diagnostics.push(diagnostic);
        });
        
				this.diagnosticCollection.set(textDocument.uri, diagnostics);	
			});
		}
	}
	
  private runCodeAction(document: vscode.TextDocument, range: vscode.Range, message:string): any 
  {
		/*let fromRegex:RegExp = /.*Replace:(.*)==>.* /g
		let fromMatch:RegExpExecArray = fromRegex.exec(message.replace(/\s/g, ''));
		let from = fromMatch[1];
		let to:string = document.getText(range).replace(/\s/g, '')
    if (from === to) 
    {
			let newText = /.*==>\s(.*)/g.exec(message)[1]
			let edit = new vscode.WorkspaceEdit();
			edit.replace(document.uri, range, newText);
			return vscode.workspace.applyEdit(edit);
    } 
    else 
    {
			vscode.window.showErrorMessage("The suggestion was not applied because it is out of date. You might have tried to apply the same edit twice.");
    }
    */
  }
}