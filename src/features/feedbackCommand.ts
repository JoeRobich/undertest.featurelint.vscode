const open = require('open');

export function feedbackCommand(): void
{
  open('https://gitlab.com/under-test/undertest.featurelint.vscode/issues');
}