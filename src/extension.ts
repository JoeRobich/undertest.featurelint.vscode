import * as vscode from 'vscode';

import GherkinLinter from './features/gherkinLinter';
import { Constants } from './constants';
import { checkIfFeatureLintNeedsInstallingOrUpgrading } from './features/checkFeatureLintInstallStatus';
import { feedbackCommand } from './features/feedbackCommand';

export async function activate(context: vscode.ExtensionContext) 
{
	await checkIfFeatureLintNeedsInstallingOrUpgrading();
	
	const linter = new GherkinLinter();
	linter.activate(context.subscriptions);
	vscode.languages.registerCodeActionsProvider(Constants.TargetLanguageId, linter);

	const feedback = vscode.commands.registerCommand('undertest.feedback', feedbackCommand);
	context.subscriptions.push(feedback);
}

export function deactivate() {}
 