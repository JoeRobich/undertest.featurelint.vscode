export interface FeatureLintData 
{
  Type: number;
  Line: number;
  Column: number;
  Message: string;
}
