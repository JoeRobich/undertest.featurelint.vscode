import { FeatureLintResultGroupedByFile } from "./FeatureLintResultGroupedByFile";

export interface FeatureLintResponse 
{
  Successful: boolean;
  Results: FeatureLintResultGroupedByFile[];
}
