import { FeatureLintData } from './FeatureLintData';

export interface FeatureLintResultGroupedByFile 
{
  FilePath: string;
  Findings: FeatureLintData[];
}
